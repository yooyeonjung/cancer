import requests
import json
import sys
import pickle
import os

i, o, e = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
sys.stdin, sys.stdout, sys.stderr = i, o, e
from scapy.layers.dns import DNS, DNSQR, DNSRR
from mpl_toolkits.basemap import Basemap
import numpy as np
import matplotlib.pyplot as plt
hops = []
flag = False

def ReturnHops():
    if flag:
        return hops
    return False

if __name__ == "__main__":
    lons = []
    lats = []
    labels = []
    labels_num = []
    bashCommand = "tracert -w 1000 " + sys.argv[1]
    process = subprocess.Popen(bashCommand.split(), stdout = subprocess.PIPE)
    output, error = process.communicate()
    print output
    if "Unable" in output:
        sys.exit(0)
    rawdata = output.split()
    first = True
    DB_API = "3b5b36adc11d120cb34dc4a5dffcd76c412de4c4/"
    GOOGLE_API = "AIzaSyD06XcgLaZ-uviv0J3JV7N1Gux4UMlqOUk"
    for element in rawdata:
        if re.match(r"^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$",element) or re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",element):
            if first:
                first = False
                continue
            hop_data = {}
            hop_data['hop_num'] = len(hops)
            hop_data['ip_addr'] = element.strip('[').strip(']')
            data_loc = json.loads(requests.get('http://api.db-ip.com/v2/'+ DB_API + str(hop_data['ip_addr'])).text)
            if len(data_loc) != 7:
                continue
            city = data_loc['city']
            region = data_loc['stateProv']
            country = data_loc['countryName']
            lat_long = requests.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + city + '&key=' + GOOGLE_API).text
            data_geo = json.loads(lat_long)
            hop_data['latitude'] = data_geo['results'][0]['geometry']['location']['lat']
            print type(hop_data['latitude'])
            hop_data['longtitude'] = data_geo['results'][0]['geometry']['location']['lng']
            hop_data['city'] = city
            hop_data['region'] = region
            hop_data['country'] = country
            lons.append(hop_data['longtitude'])
            lats.append(hop_data['latitude'])
            labels.append(city)
            labels_num.append(str(hop_data['hop_num'])  + " - " + city)
            hops.append(hop_data)
    for ele in hops:
        print "Num: " + str(ele['hop_num'])
        print "IP: " + str(ele['ip_addr'])
        print "Lat: " + str(ele['latitude'])
        print "Lon: " + str(ele['longtitude'])
        print "Location: " + ele['city'] + ", " + ele['region'] + ", " + ele['country']
        print "___________________________"
    flag = True
    map = Basemap(llcrnrlat=-90,urcrnrlat=90,\
                llcrnrlon=-180,urcrnrlon=180,resolution='c')
    map.bluemarble()
    # draw parallels and meridians.
    map.drawmapboundary(fill_color='aqua')
    plt.title("Map")

    x,y = map(lons, lats)
    map.plot(x, y, 'rv', markersize=10)
    for label, xpt, ypt in zip(labels, x, y):
        plt.text(xpt, ypt, label)
        print label

    for i in range (0, len(labels)-1):
        map.drawgreatcircle(lons[i],lats[i],lons[i+1],lats[i+1],linewidth=1,color='r')

    plt.show()

